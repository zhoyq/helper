/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper;

import org.junit.Assert;
import org.junit.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.awt.*;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * @author 刘路 <a href="mailto:feedback@zhoyq.com">feedback@zhoyq.com</a>
 * @date 2018/11/23
 */
public class CodeTest {

    @Test
    public void randomColor(){
        Color color = Code.randomColor(0,100);
        Assert.assertTrue(color.getBlue() <= 100 && color.getBlue() >= 0);
        Assert.assertTrue(color.getRed() <= 100 && color.getRed() >= 0);
        Assert.assertTrue(color.getGreen() <= 100 && color.getGreen() >= 0);
    }

    @Test
    public void md5() throws NoSuchAlgorithmException {
        String abc = Code.md5("abc");
        Assert.assertEquals("900150983CD24FB0D6963F7D28E17F72",
                abc);
    }

    @Test
    public void hmacMd5() throws NoSuchAlgorithmException {
        String abc = Code.hmacMd5("123","abc");
        Assert.assertEquals("FFB7C0FC166F7CA075DFA04D59AED232",
                abc);
    }


    @Test
    public void sm3(){
        String abc = Code.sm3("abc");
        Assert.assertEquals("66C7F0F462EEEDD9D1F2D46BDC10E4E24167C4875CF2F7A2297DA02B8F4BA8E0",
                abc);
        String abcWithKey = Code.sm3("123","abc");
        Assert.assertEquals("0EB169EE3AAAEC33DDAE9CE2B68017B8208E407E107A3FF865E72C04970360DE",
                abcWithKey);
    }

    @Test
    public void rsa(){

        // RSA加密
        // 甲方构建密钥对儿，将公钥公布给乙方，将私钥保留。
        // 甲方使用私钥加密数据，然后用私钥对加密后的数据签名，发送给乙方签名以及加密后的数据；
        // 乙方使用公钥、签名来验证待解密数据是否有效，如果有效使用公钥对数据解密。
        // 乙方使用公钥加密数据，向甲方发送经过加密后的数据；甲方获得加密数据，通过私钥解密。

        KeyPair p = Code.genRSAKeyPair(1024);
//        System.out.println(p.getPublic());
//        System.out.println(p.getPrivate());

//        System.out.println("加密数据");
        String inputStr = "abc";
//        System.out.println(inputStr);
        byte[] data = inputStr.getBytes();

//        System.out.println("公钥加密-私钥解密");
        byte[] encodeData = Code.rsaEncodeByPublicKey(data, p.getPublic().getEncoded());
        byte[] decodeData = Code.rsaDecodeByPrivateKey(encodeData,p.getPrivate().getEncoded());
//        System.out.println(new String(decodeData));
        Assert.assertEquals(inputStr, new String(decodeData));

//        System.out.println("私钥加密-公钥解密");
        byte[] encodeData2 = Code.rsaEncodeByPrivateKey(data, p.getPrivate().getEncoded());
        byte[] decodeData2 = Code.rsaDecodeByPublicKey(encodeData2,p.getPublic().getEncoded());
//        System.out.println(new String(decodeData2));
        Assert.assertEquals(inputStr, new String(decodeData2));

//        System.out.println("私钥签名-公钥验证");
        byte[] sign = Code.rsaSign(encodeData2,  p.getPrivate().getEncoded());
        boolean status = Code.rsaVerify(encodeData2, p.getPublic().getEncoded(), sign);
//        System.out.println(status);
        Assert.assertTrue(status);
    }

    @Test
    public void sm4() throws IllegalBlockSizeException,
            InvalidKeyException, BadPaddingException,
            NoSuchAlgorithmException, NoSuchPaddingException, NoSuchProviderException {
        String key = Code.genSm4Key(128);
        String input = "abcdefg";
        String res = Code.sm4Encode(key, input);
        String out = Code.sm4Decode(key, res);
        Assert.assertEquals(input, out);
    }

}
