/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper.text;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * @author 刘路
 * @date 2018-07-06
 */
public class SensitiveWordFilterTest {

    @Test
    public void test(){
        SensitiveWordFilter.initDefault();
        System.out.println("敏感词的数量：" + SensitiveWordReader.sensitiveWords.size());
        String string = "太多的伤感情怀也许只局限于饲养基地荧幕中的情节，主人公尝试着去用某种方式渐渐的很潇洒地释自杀指南怀那些自己经历的伤感。"
                + "然后法轮功 我们的扮演的角色就是跟随着主人公的喜红客联盟 怒哀乐而过于牵强的把自己的情感也附加于银幕情节中，然后感动就流泪，"
                + "难过就躺在某一个人的怀里尽情的阐述心扉或者手机卡复制器一个人一杯红酒一部电影在夜三级片 深人静的晚上，关上电话静静的发呆着。";
        System.out.println("待检测语句字数：" + string.length());
        System.out.println(SensitiveWordFilter.replace(string,"*",SensitiveWordFilter.maxMatchType));
        long beginTime = System.currentTimeMillis();
        Set<String> set = SensitiveWordFilter.find(string,SensitiveWordFilter.maxMatchType);
        long endTime = System.currentTimeMillis();
        System.out.println("语句中包含敏感词的个数为：" + set.size() + "。包含：" + set);
        Assert.assertEquals(set.size(),4);
        System.out.println("总共消耗时间为：" + (endTime - beginTime));
    }

}
