/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Hashtable;

/**
 * 用于生成二维码
 * @author 刘路
 */
public class QRHelper {
  
  private static String format = "png";   
  
  /**
   * 将二维码输出到流
   * @param text   内容
   * @param width  宽度
   * @param height 高度
   * @param os     输出流
   * @throws WriterException
   * @throws IOException
   */
  public static void toStream(String text,int width,int height,OutputStream os) throws WriterException, IOException{
    Hashtable<EncodeHintType,String> hints= new Hashtable<EncodeHintType,String>();   
    hints.put(EncodeHintType.CHARACTER_SET, "utf-8");   
    BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, width, height,hints); 
    MatrixToImageWriter.writeToStream(bitMatrix, format, os);  
  }
  /**
   * 将二维码输出到文件
   * @param text   内容
   * @param width  宽度
   * @param height 高度
   * @param path   输出路径
   * @throws WriterException
   * @throws IOException
   */
  public static void toPath(String text,int width,int height,String path) throws WriterException, IOException{
    Hashtable<EncodeHintType,String> hints= new Hashtable<EncodeHintType,String>();   
    hints.put(EncodeHintType.CHARACTER_SET, "utf-8");   
    BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, width, height,hints);
    Path apath = FileSystems.getDefault().getPath(path);
    MatrixToImageWriter.writeToPath(bitMatrix, format, apath );   
  }
  
}


