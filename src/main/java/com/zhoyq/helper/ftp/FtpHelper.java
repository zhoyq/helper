/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper.ftp;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

/**
 * 简单的GTP帮助类
 * @author 刘路
 */
public class FtpHelper {
  
  public static Logger logger = Logger.getLogger(FtpHelper.class);
  
  /** 
   * 获取FTPClient对象 
   * @param ftpHost FTP主机服务器 
   * @param ftpPassword FTP 登录密码 
   * @param ftpUserName FTP登录用户名 
   * @param ftpPort FTP端口 默认为21 
   * @return 
   */  
  public static FTPClient getFTPClient(String ftpHost,  String ftpUserName,
      String ftpPassword,   int ftpPort) {  
      FTPClient ftpClient = null;  
      try {  
          ftpClient = new FTPClient();
          // 连接FTP服务器
          ftpClient.connect(ftpHost, ftpPort);
          // 登陆FTP服务器
          ftpClient.login(ftpUserName, ftpPassword);
          if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {  
              logger.info("未连接到FTP，用户名或密码错误。");  
              ftpClient.disconnect();  
          } else {  
              logger.info("FTP连接成功。");  
          }  
      } catch (SocketException e) {  
          e.printStackTrace();  
          logger.info("FTP的IP地址可能错误，请正确配置。");  
      } catch (IOException e) {  
          e.printStackTrace();  
          logger.info("FTP的端口错误,请正确配置。");  
      }  
      return ftpClient;  
  }
  
  /**
   * 去 服务器的FTP路径下上读取文件  远程文件路径FTP
   * @param ftpClient
   * @param ftpPath
   * @param fileName
   * @return
   */
  public static String read(FTPClient ftpClient, String ftpPath,  String fileName) {  
      StringBuffer resultBuffer = new StringBuffer();  
      InputStream in = null;   
      logger.info("开始读取绝对路径" + ftpPath + "文件!");  
      try {
          // 中文支持
          ftpClient.setControlEncoding("UTF-8");
          ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);  
          ftpClient.enterLocalPassiveMode();  
          ftpClient.changeWorkingDirectory(ftpPath);  
          in = ftpClient.retrieveFileStream(fileName);  
      } catch (FileNotFoundException e) {  
          logger.error("没有找到" + ftpPath + "文件");  
          e.printStackTrace();  
          return "下载配置文件失败，请联系管理员.";  
      } catch (SocketException e) {  
          logger.error("连接FTP失败.");  
          e.printStackTrace();  
      } catch (IOException e) {  
          e.printStackTrace();  
          logger.error("文件读取错误。");  
          e.printStackTrace();  
          return "配置文件读取失败，请联系管理员.";  
      }  
      if (in != null) {  
          BufferedReader br = new BufferedReader(new InputStreamReader(in));  
          String data = null;  
          try {  
              while ((data = br.readLine()) != null) {  
                  resultBuffer.append(data + "\n");  
              }  
          } catch (IOException e) {  
              logger.error("文件读取错误。");  
              e.printStackTrace();  
              return "配置文件读取失败，请联系管理员.";  
          }finally{  
              try {  
                  ftpClient.disconnect();  
              } catch (IOException e) {  
                  e.printStackTrace();  
              }  
          }  
      }else{  
          logger.error("in为空，不能读取。");  
          return "配置文件读取失败，请联系管理员.";  
      }  
      return resultBuffer.toString();  
  }  
  
  /**
   * 本地上传文件到FTP服务器 
   * @param ftpClient
   * @param ftpPath
   * @param fileContent
   */
  public static void upload(FTPClient ftpClient, String ftpPath, byte[] fileContent) {  
      logger.info("开始上传文件到FTP.");  
      try {    
          // 设置PassiveMode传输  
          ftpClient.enterLocalPassiveMode();  
          // 设置以二进制流的方式传输  
          ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);  
          InputStream in = new ByteArrayInputStream(fileContent);  
          ftpClient.storeFile(ftpPath, in);  
          in.close();  
          logger.info("上传文件" + ftpPath + "到FTP成功!");  
      } catch (Exception e) {  
          e.printStackTrace();  
      }finally{  
          try {  
              ftpClient.disconnect();  
          } catch (IOException e) {  
              e.printStackTrace();  
          }  
      }  
  }  

}
