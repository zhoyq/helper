/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author 刘路
 */
public class SerializableHelper {
  
  private static Logger log = LoggerFactory.getLogger(SerializableHelper.class);

  /**
   * @param data
   * @param <T>
   * @return
   */
  public static <T> T toObj(byte[] data) {
    try{
      ByteArrayInputStream bais = new ByteArrayInputStream(data);
      ObjectInputStream ois = new ObjectInputStream(bais);
      T t = (T) ois.readObject();
      ois.close();
      bais.close();
      return t;
    }catch(Exception e){ 
      log.error(e.getMessage()); 
      return null;
    } 
  }

  /**
   * @param obj
   * @param <T>
   * @return
   */
  public static <T> byte[] toBytes(T obj){
    byte[] res = null;
    try{
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(obj); 
      res = baos.toByteArray().clone();
      oos.close();
      baos.close();
      return res;
    }catch(Exception e){ 
      log.error(e.getMessage()); 
      return null;
    }  
  }
   
}
