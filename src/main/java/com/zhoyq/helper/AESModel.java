/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper;

/**
 * 加密模式
 * 目前实现AES的两种 ECB CBC
 * 详情 http://www.cnblogs.com/happyhippy/archive/2006/12/23/601353.html
 * @author 刘路
 */
public enum AESModel {
  /**
   * AES_ECB
   */
  AES_ECB("AES"),
  /**
   * AES_ECB_PKCS5Padding
   */
  AES_ECB_PKCS5Padding("AES/ECB/PKCS5Padding"),
  /**
   * AES_CBC_PKCS5Padding
   */
  AES_CBC_PKCS5Padding("AES/CBC/PKCS5Padding"),
  /**
   * AES/CBC/NoPadding 要求
   * 密钥必须是16位的；Initialization vector (IV) 必须是16位
   * 待加密内容的长度必须是16的倍数，如果不是16的倍数，就会出如下异常：
   * javax.crypto.IllegalBlockSizeException: Input length not multiple of 16 bytes
   * 
   *  由于固定了位数，所以对于被加密数据有中文的, 加、解密不完整
   *  
   *  可以看到，在原始数据长度为16的整数n倍时，假如原始数据长度等于16*n，则使用NoPadding时加密后数据长度等于16*n，
   *  其它情况下加密数据长度等于16*(n+1)。在不足16的整数倍的情况下，假如原始数据长度等于16*n+m[其中m小于16]，
   *  除了NoPadding填充之外的任何方式，加密数据长度都等于16*(n+1).
   */
  AES_CBC("AES/CBC/NoPadding");

  private String value;

  AESModel(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }
}
