/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author 刘路
 */
public class ClassHelper { 
	/**
	 * 获取包内所有类
	 * @param pkgName 包名
	 * @param isRecursive 是否递归子包
	 * @return
	 */
	public static List<Class<?>> getClassList(String pkgName, boolean isRecursive) {
		List<Class<?>> classList = new ArrayList<Class<?>>();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try {
			// 按文件的形式去查找
			String strFile = pkgName.replaceAll("\\.", "/");
			Enumeration<URL> urls = loader.getResources(strFile);
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				if (url != null) {
					String protocol = url.getProtocol();
					String pkgPath = url.getPath();
					if ("file".equals(protocol)) {
						// 本地自己可见的代码
						findClassName(classList, pkgName, pkgPath, isRecursive);
					} else if ("jar".equals(protocol)) {
						// 引用第三方jar的代码
						findClassName(classList, pkgName, url, isRecursive);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return classList;
	}

	private static void findClassName(List<Class<?>> clazzList, String pkgName, String pkgPath, boolean isRecursive) {
		if (clazzList == null) {
			return;
		}
		// 过滤出.class文件及文件夹
		File[] files = filterClassFiles(pkgPath);
		if (files != null) {
			for (File f : files) {
				String fileName = f.getName(); 
				if (f.isFile()) {
					// .class 文件的情况
					String clazzName = getClassName(pkgName, fileName);
					addClassName(clazzList, clazzName);
				} else {
					// 文件夹的情况
					if (isRecursive) {
						// 需要继续查找该文件夹/包名下的类
						String subPkgName = pkgName + "." + fileName;
						String subPkgPath = pkgPath + "/" + fileName;
						findClassName(clazzList, subPkgName, subPkgPath, true);
					}
				}
			}
		}
	}

	/**
	 * 第三方Jar类库的引用。<br/>
	 * @throws IOException
	 */
	private static void findClassName(List<Class<?>> clazzList, String pkgName, URL url, boolean isRecursive) throws IOException {
		JarURLConnection jarURLConnection = (JarURLConnection) url.openConnection();
		JarFile jarFile = jarURLConnection.getJarFile();
		Enumeration<JarEntry> jarEntries = jarFile.entries();
		while (jarEntries.hasMoreElements()) {
			JarEntry jarEntry = jarEntries.nextElement();
			// 类似：sun/security/internal/interfaces/TlsMasterSecret.class
			String jarEntryName = jarEntry.getName();
			String clazzName = jarEntryName.replace("/", ".");
			int endIndex = clazzName.lastIndexOf(".");
			String prefix = null;
			if (endIndex > 0) {
				String prefix_name = clazzName.substring(0, endIndex);
				endIndex = prefix_name.lastIndexOf(".");
				if (endIndex > 0) {
					prefix = prefix_name.substring(0, endIndex);
				}
			}
			if (prefix != null && jarEntryName.endsWith(".class")) {
				if (prefix.equals(pkgName)) {
					addClassName(clazzList, clazzName);
				} else if (isRecursive && prefix.startsWith(pkgName)) {
					// 遍历子包名：子类
					addClassName(clazzList, clazzName);
				}
			}
		}
	}

	private static File[] filterClassFiles(String pkgPath) {
		if (pkgPath == null) {
			return null;
		}
		// 接收 .class 文件 或 类文件夹
		return new File(pkgPath).listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return (file.isFile() && file.getName().endsWith(".class")) || file.isDirectory();
			}
		});
	}

	private static String getClassName(String pkgName, String fileName) {
		int endIndex = fileName.lastIndexOf(".");
		String clazz = null;
		if (endIndex >= 0) {
			clazz = fileName.substring(0, endIndex);
		}
		String clazzName = null;
		if (clazz != null) {
			clazzName = pkgName + "." + clazz;
		}
		return clazzName;
	}

	private static void addClassName(List<Class<?>> clazzList, String clazzName) {
		if (clazzList != null && clazzName != null) {
			Class<?> clazz = null;
            
			// 判断clazzName结尾 并去除
			if(clazzName.endsWith(".class")){
				clazzName = clazzName.substring(0, clazzName.length()-6);
			} 
			
			try {
				clazz = Class.forName(clazzName);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
			if (clazz != null) {
			  clazzList.add(clazz);
			}
		}
	}
}
