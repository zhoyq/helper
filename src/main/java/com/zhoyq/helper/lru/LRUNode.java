/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper.lru;

/**
 * 近期最少使用列表的节点
 * @author 刘路
 */
public class LRUNode implements LRUAble {

  /**
   * 前一个节点
   */
  protected LRUAble prev;
  /**
   * 后一个节点
   */
  protected LRUAble next;

  public LRUNode() {
    this.prev = null;
    this.next = null;
  }

  public LRUNode(LRUAble prev, LRUAble next) {
    this.prev = prev;
    this.next = next;
  }

  @Override
  public LRUAble getNext() {
    return next;
  }
  @Override
  public LRUAble getPrev() {

    return prev;
  }
  @Override
  public void setPrev(LRUAble prev) {
    this.prev = prev;
  }
  @Override
  public void setNext(LRUAble next) {
    this.next = next;
  }
}
