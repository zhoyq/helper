/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper.lru;

/**
 * 近期最少使用列表
 * @author 刘路
 */
public abstract class LRUList {
  protected LRUNode head;
  protected LRUNode tail;

  public LRUList() {
    this.head = new LRUNode();
    this.tail = new LRUNode();
    head.prev = null;
    head.next = tail;
    tail.prev = head;
    tail.next = null;
  }

  /**
   * 移动一个新的或者已经存在的节点到列表前端
   * @param node 节点
   */
  public abstract void toHead(LRUAble node);

  /**
   * 移动一个新的或者已经存在的节点到列表末端
   * @param node 节点
   */
  public abstract void toTail(LRUAble node);

  /**
   * 节点在列表中则移除 不在什么也不做 当节点被移除的时候 两端点的链接空
   * @param node 要移除的节点
   * @return 返回同一个节点
   */
  public abstract LRUAble remove(LRUAble node);

  /**
   * 获取尾节点
   * @return 尾节点 如果列表是空则返回空
   */
  public abstract LRUAble getTail();

  /**
   * 获取头节点
   * @return 头节点 如果列表是空则返回空
   */
  public abstract LRUAble getHead();

  /**
   * 获取尾节点并且移除列表
   * @return 尾节点 如果列表是空则返回空
   */
  public abstract LRUAble removeTail();

  /**
   * 获取下一个节点
   * @return 下一个节点 如果列表是空则返回空
   */
  abstract public LRUAble getNext(LRUAble node);

  /**
   * 获取上一个节点
   * @return 上一个节点 如果列表是空则返回空
   */
  abstract public LRUAble getPrev(LRUAble node);

}
