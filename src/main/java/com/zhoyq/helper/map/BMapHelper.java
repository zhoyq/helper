/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper.map;

import com.zhoyq.helper.DataCrawlingHelper;
import com.zhoyq.helper.HttpMethod;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BMapHelper {
  /**
   * 地图服务的key
   */
  private static String webServiceKey = "";
  private final static String geocodeUrl = "http://api.map.baidu.com/geocoder/v2/";
  
  /**
   * 配置项 用于全局配置
   * 这个配置整个应用只配置一次就够了
   * 必须先初始化key在调用这个帮助类
   * @param key 
   */
  public final static void key(String key){
    webServiceKey = key;
  }
  
  /**
   * 获取参数
   * @return
   */
  private final static Map<String,String> newParams(){
    Map<String,String> params = new HashMap<String,String>();
    params.put("ak", webServiceKey);
    params.put("output", "json");
    return params;
  } 
  
  /**
   * 地理编码查询
   * 单个key支持6000次/天调用
   * @param address  省+市+区+街道+门牌号
   * @return 116.397499,39.908722
   */
  public final static String geocode(String address){
    Map<String,String> params = newParams();
    params.put("address", address);
    String json = DataCrawlingHelper.getData(geocodeUrl, params, HttpMethod.GET, "UTF-8");
    JSONObject jsonObj = JSONObject.parseObject(json);
    if(jsonObj.getString("status").equals("0")){ 
      JSONObject jsonObjj = jsonObj.getJSONObject("result").getJSONObject("location");
      if(jsonObjj!=null){
        String lng = jsonObjj.getString("lng");
        String lat = jsonObjj.getString("lat");
        return lng+","+lat;
      } 
    }  
    return null;
  }
  
  /**
   * 逆地理编码查询
   * 单个key支持6000次/天调用
   * @param latlng 例：<code>39.908722,116.397499</code>
   * @return 地址
   */
  public final static String regeocode(String latlng){
    Map<String,String> params = newParams();
    params.put("location", latlng); 
    String json = DataCrawlingHelper.getData(geocodeUrl, params, HttpMethod.GET, "UTF-8");
    JSONObject jsonObj = JSONObject.parseObject(json);
    if(jsonObj.getString("status").equals("0")){
      String res = jsonObj.getJSONObject("result").getString("formatted_address");
      return res;
    }  
    return null;
  }
  
}
