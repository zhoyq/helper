/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper.map;

import com.zhoyq.helper.DataCrawlingHelper;
import com.zhoyq.helper.HttpMethod;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OpenStreetMapHelper { 
  
  public static final String geocodeUrl = "http://nominatim.openstreetmap.org/reverse";
  
  /**
   * 获取参数
   * @return
   */
  private final static Map<String,String> newParams(){
    Map<String,String> params = new HashMap<String,String>();
    params.put("format", "json");
    params.put("zoom", "18");
    params.put("addressdetails", "1");
    return params;
  } 
  
  private final static String format(String raddr){
    if(raddr==null) return null;
    StringBuilder addr = new StringBuilder();
    String[] addrArr = raddr.split(",");
    for(int i=addrArr.length-1;i>=0;i--){
      addr.append(addrArr[i].trim());
    }
    return addr.toString();
  }
  
  /**
   * 逆地理编码查询
   * 无限制
   * @param lat  
   * @param lng
   * @return 地址
   */
  public final static String regeocode(String lng,String lat){
    Map<String,String> params = newParams();
    params.put("lat", lat); 
    params.put("lon", lng); 
    String json = DataCrawlingHelper.getData(geocodeUrl, params, HttpMethod.GET, "UTF-8");
    JSONObject jsonObj = JSONObject.parseObject(json);
    String res = jsonObj.getString("display_name");
    return format(res);
  } 
}
