/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper;

import java.util.List;

/**
 * zip压缩实体封装
 * {@link CompressHelper}
 * @author 刘路
 */
public class ZipEntity {

    private boolean isFile;
    private String name;
    private byte[] data;
    private List<ZipEntity> subFiles;

    public boolean isFile() {
        return isFile;
    }

    public void setFile(boolean file) {
        isFile = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public List<ZipEntity> getSubFiles() {
        return subFiles;
    }

    public void setSubFiles(List<ZipEntity> subFiles) {
        this.subFiles = subFiles;
    }
}
