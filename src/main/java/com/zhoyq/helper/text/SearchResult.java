/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper.text;

import java.util.List;

/**
 * @author 刘路
 */
public class SearchResult<T> {

    private List<T> list;
    private Integer pageNumber;
    private Integer pageSize;
    private Integer pageCount;
    private Integer count;

    public SearchResult(List<T> list, Integer pageNumber, Integer pageSize, Integer pageCount, Integer count) {
        this.list = list;
        this.pageCount = pageCount;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public List<T> getList() {
        return list;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}

