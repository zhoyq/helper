/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper.text;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @author 刘路
 */
public class SensitiveWordReader {

    public static Logger log = LoggerFactory.getLogger(SensitiveWordReader.class);
    /**
     * 敏感词库
     */
    public static Map<String,Map> sensitiveWords = new HashMap<>();
    /**
     * 读取文件
     *
     * 文件结构 每行一个敏感词
     *
     * @param file
     * @return
     */
    private static Set<String> read(File file) {
        Set<String> set = null;
        try {
            InputStreamReader read = new InputStreamReader(new FileInputStream(file),"UTF-8");
            if(file.isFile() && file.exists()){
                set = new HashSet<>();
                BufferedReader bufferedReader = new BufferedReader(read);
                while(bufferedReader.ready()){
                    String line = bufferedReader.readLine();
                    if(line!=null && !line.trim().equalsIgnoreCase("")){
                        set.add(line);
                    }
                }
            }
            read.close();
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
        return set;
    }

    /**
     * 添加敏感词库
     * @param file
     */
    public static void add(File file){
        Map nowMap;
        Set<String> keyWordSet = read(file);
        // 迭代keyWordSet
        Iterator<String> iterator = keyWordSet.iterator();
        while(iterator.hasNext()){
            // 关键字
            String key = iterator.next();
            nowMap = sensitiveWords;
            for(int i = 0 ; i < key.length() ; i++){
                // 转换成char型
                char keyChar = key.charAt(i);
                // 获取
                Object wordMap = nowMap.get(String.valueOf(keyChar));
                // 如果存在该key，直接赋值
                if(wordMap != null){
                    nowMap = (Map)wordMap;
                } else{
                    // 不存在则，则构建一个map，同时将isEnd设置为0，因为他不是最后一个
                    Map<String,String> newWorMap = new HashMap<>(1);
                    newWorMap.put("isEnd", "0");
                    String buf = String.valueOf(keyChar);
                    nowMap.put(buf, newWorMap);
                    nowMap = newWorMap;
                }
                if(i == key.length() - 1){
                    // 最后一个
                    nowMap.put("isEnd", "1");
                }
            }
        }
    }

}
