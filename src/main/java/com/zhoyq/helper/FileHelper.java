/*
 *   Copyright (c) 2018. 刘路 All rights reserved
 *   版权所有 刘路 并保留所有权利 2018.
 *   ===============================================================
 *   这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 *   使用。不允许对程序代码以任何形式任何目的的再发布。如果项目发布携带作者
 *   认可的特殊 LICENSE 则按照 LICENSE 执行，废除上面内容。请保留原作者信息。
 *   ================================================================
 *   刘路（feedback@zhoyq.com）于 2018. 创建
 *   http://zhoyq.com
 */

package com.zhoyq.helper;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * @author 刘路
 */
public class FileHelper {
  
  private final static Logger log = LoggerFactory.getLogger(FileHelper.class);

  /**
   * 注意：读取内容不一定完全一致
   * @param calsspath
   * @return
   */
  public final static String toString(String calsspath){
    if(calsspath==null) {
      return null;
    }
    try{
      BufferedReader br = new BufferedReader(new InputStreamReader(FileHelper.class.getResourceAsStream(calsspath)));
      StringBuilder res = new StringBuilder();
      while(br.ready()){
        res.append(br.readLine() + "\n");
      }
      br.close();
      return res.toString();
    }catch(Exception e){
      log.warn(e.getMessage());
      return null;
    }
  }

  /**
   * @param file
   * @return
   */
  public final static byte[] toByteArr(File file){
    if(file==null) {
      return null;
    }
    try{
      InputStream is = new FileInputStream(file);
      byte[] res = new byte[0];
      byte[] buf = new byte[1024];
      int len;
      while(( len = is.read(buf) ) != -1){
        res = ByteArrHelper.union(res,ByteArrHelper.subByte(buf,0,len));
      }
      return res;
    }catch(Exception e){
      log.warn(e.getMessage());
      return null;
    }
  }

  /**
   * 注意：读取内容不一定完全一致
   * @param file
   * @return
   */
  public final static String toString(File file){
    if(file==null) {
      return null;
    }
    try{
      BufferedReader br = new BufferedReader(new FileReader(file));
      StringBuilder res = new StringBuilder();
      while(br.ready()){
        res.append(br.readLine()+"\n");
      }
      br.close();
      return res.toString();
    }catch(Exception e){
      log.warn(e.getMessage());
      return null;
    }
  }

  /**
   * @param data
   * @param path
   */
  public final static void save(String data,String path){
    save(data.getBytes(),path);
  }

  /**
   * @param data
   * @param path
   */
  public final static void save(byte[] data,String path){
    File file = new File(path);
    save(data,file);
  }

  /**
   * @param data
   * @param file
   */
  public final static void save(byte[] data,File file){
    try {
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(data);
      fos.close();
    } catch (IOException e) {
      log.warn(e.getMessage());
    }
  }

  /**
   * @param o
   * @param d
   * @throws Exception
   */
  public final static void copy(File o,File d) throws Exception {
    if(o.isDirectory() && d.isDirectory()){
      for(File f:o.listFiles()){
        File dest = new File(d.getAbsolutePath()+File.separator+f.getName());
        if(f.isDirectory()) {
          dest.mkdir();
        }
        copy(f,dest);
      }
    }else if(!o.isDirectory() && d.isDirectory()){
      File dest = new File(d.getAbsolutePath()+File.separator+o.getName());
      try{
        if(!dest.exists()) {
          dest.createNewFile();
        }
      }catch(Exception e){}
      copy(o,dest);
    }else if(!o.isDirectory() && !d.isDirectory()){
      InputStream is = new FileInputStream(o);
      OutputStream os = new FileOutputStream(d);
      int len = 0;
      byte[] buf = new byte[1024];
      while(( len = is.read(buf)) != -1){
        os.write(buf,0,len);
      }
      is.close();
      os.close();
    }
  }

  /**
   * 注意：由于权限 有不能删除的情况
   * @param o
   */
  public final static void del(File o){
    if(o.isDirectory()){
      for(File f:o.listFiles()){
        del(f);
      }
    }
    o.delete();
  }

}
